﻿DROP DATABASE IF EXISTS m3u3p3;
CREATE DATABASE IF NOT EXISTS m3u3p3;
USE m3u3p3;

CREATE TABLE IF NOT EXISTS zonaurbana (
  NombreZona int(11) NOT NULL AUTO_INCREMENT,
  Categoria char(10) DEFAULT NULL,
  PRIMARY KEY (NombreZona)
);

CREATE TABLE IF NOT EXISTS bloquecasas (
  Calle varchar(20) NOT NULL,
  Numero int(11) NOT NULL,
  Npisos int(11) DEFAULT NULL,
  NombreZona int(11) DEFAULT NULL,
  PRIMARY KEY (Calle, Numero),
  CONSTRAINT fk_ZonaUrbana_BloqueCasas FOREIGN KEY (NombreZona)
  REFERENCES zonaurbana (NombreZona) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS casaparticular (
  NombreZona int(11) NOT NULL,
  Numero int(11) NOT NULL,
  M2 int(11) DEFAULT NULL,
  PRIMARY KEY (NombreZona, Numero),
  CONSTRAINT fk_ZonaUrbana_CasaParticular FOREIGN KEY (NombreZona)
  REFERENCES zonaurbana (NombreZona) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS piso (
  Calle varchar(20) NOT NULL,
  Numero int(11) NOT NULL,
  Planta int(11) NOT NULL,
  Puerta char(1) NOT NULL,
  M2 int(11) DEFAULT NULL,
  PRIMARY KEY (Calle, Numero, Planta, Puerta),
  CONSTRAINT fk_BloqueCasas_Piso FOREIGN KEY (Calle, Numero)
  REFERENCES bloquecasas (Calle, Numero) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS persona (
  DNI varchar(9) NOT NULL,
  Nombre varchar(15) DEFAULT NULL,
  Edad int(2) DEFAULT NULL,
  NombreZona int(11) DEFAULT NULL,
  NumCasa int(11) DEFAULT NULL,
  Calle varchar(20) DEFAULT NULL,
  NumBloque int(11) DEFAULT NULL,
  Planta int(11) DEFAULT NULL,
  Puerta char(1) DEFAULT NULL,
  PRIMARY KEY (DNI),
  CONSTRAINT fk_CasaParticular_Persona FOREIGN KEY (NombreZona, NumCasa)
  REFERENCES casaparticular (NombreZona, Numero) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_Piso_Persona FOREIGN KEY (Calle, NumBloque, Planta, Puerta)
  REFERENCES piso (Calle, Numero, Planta, Puerta) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS poseec (
  DNI varchar(9) NOT NULL,
  NombreZona int(11) NOT NULL,
  NumCasa int(11) NOT NULL,
  PRIMARY KEY (DNI, NombreZona, NumCasa),
  CONSTRAINT fk_CasaParticular_PoseeC FOREIGN KEY (NombreZona, NumCasa)
  REFERENCES casaparticular (NombreZona, Numero) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_Persona_PoseeC FOREIGN KEY (DNI)
  REFERENCES persona (DNI) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS poseep (
  DNI varchar(9) NOT NULL,
  Calle varchar(20) NOT NULL,
  NumBloque int(11) NOT NULL,
  Planta int(11) NOT NULL,
  Puerta char(1) NOT NULL,
  PRIMARY KEY (DNI, Calle, NumBloque, Planta, Puerta),
  CONSTRAINT fk_Persona_PoseeP FOREIGN KEY (DNI)
  REFERENCES persona (DNI) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_Piso_PoseeP FOREIGN KEY (Calle, NumBloque, Planta, Puerta)
  REFERENCES piso (Calle, Numero, Planta, Puerta) ON DELETE CASCADE ON UPDATE CASCADE
);

/**
  Ejercicio 2
  Crear un procedimiento al cual le pasas un numero. 
  Debe comprobar si ese valor pasado se encuentra en el campo 
  nombrezona en la tabla zona urbana.
**/

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 (numero int)
BEGIN
 IF numero IN (SELECT
        NombreZona
      FROM zonaUrbana) THEN
    SELECT
      'El valor se encuentra en la tabla';
 ELSE
    SELECT
      'El valor NO se encuentra en la tabla';
 END IF;
END//
DELIMITER ;

CALL ejercicio2(7);

-- otra version

INSERT INTO ZonaUrbana(NombreZona) VALUES ('105'),('55'),('619');
SELECT * FROM ZonaUrbana;

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(IN arg1 int)
  BEGIN
  IF ((SELECT COUNT(*) FROM zonaUrbana WHERE LOCATE(arg1,NombreZona)>0)>0) 
    THEN
    SELECT
      'El valor se encuentra en la tabla';
 ELSE
    SELECT
      'El valor NO se encuentra en la tabla';
 END IF;
    SELECT COUNT(*) FROM ZonaUrbana WHERE LOCATE(5, NombreZona)>0;
  END//
DELIMITER ;

CALL ej2(1);


DELIMITER $$

CREATE OR REPLACE PROCEDURE ejercicio2 (arg int)
BEGIN
  DECLARE v1 int; -- variable para cursor
  -- variable para contar registros 
  -- e imprimir el numero de registro donde encuentre nuestro valor
  DECLARE r int DEFAULT 1; 
  -- variable para utilizar como señal de salida del bucle 
  -- cuando encuentre el valor o 
  -- cuando llegue el final de la tabla a recorrer
  DECLARE llave int DEFAULT 1; 
  -- cursor para moverme por la tabla
  DECLARE c1 CURSOR FOR
  SELECT
    zu.NombreZona
  FROM ZonaUrbana zu;
  
  -- control excepciones 
  -- para utilizar el cursor
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET llave = 0;
  
  
  OPEN c1;
  REPEAT
    FETCH c1 INTO v1;

    IF (v1 = arg) THEN
      SELECT
        CONCAT('Valor encontrado en registro ', r) salida;
      SET llave = 0;
    /*ELSE
        SELECT CONCAT('no encontrado ', r) salida;*/
    END IF;
    SET r = r + 1;
  UNTIL (llave <> 1)
  END REPEAT;
  CLOSE c1;
END
$$

/**
  Ejercicio 3
  Crear un procedimiento, llamarlo p2, que introduzca valores 
  en la tabla zona_urbana   
**/


DELIMITER $$
CREATE PROCEDURE p2 (arg char(10))
BEGIN
  INSERT INTO ZonaUrbana
    VALUES (DEFAULT, arg);
-- SELECT 'valor insertado con exito' salida;
END
$$
DELIMITER ;

CALL p2('categoria1');
SELECT * FROM zonaurbana z;

/**
  Ejercicio 4
  Crear una función, llamarla f3, que cuente el numero de 
  registros de la tabla ZonaUrbana
**/

DELIMITER //
CREATE OR REPLACE FUNCTION f3 ()
RETURNS int
BEGIN
  RETURN (SELECT COUNT(*) FROM zonaUrbana);
END//

DELIMITER ;

SELECT f3();

-- otra version con variables

DELIMITER //
CREATE OR REPLACE FUNCTION f3a ()
RETURNS int
BEGIN
  DECLARE resultado int;
  
  SELECT COUNT(*) INTO resultado FROM zonaUrbana;

  RETURN resultado;
END//

DELIMITER ;

SELECT f3a();


/**
  Ejercicio 5
  Crear un procedimiento que utilice el valor devuelto por 
  la funcion anterior. Este procedimiento inserta un registro 
  en casaparticular siempre que haya algun registro en la 
  tabla zonaurbana
**/

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio5//
CREATE PROCEDURE ejercicio5 (nombreZona int, numero_entrada int, metros int)
BEGIN
  DECLARE numero int DEFAULT 0;

  SET numero = f3();

  -- si no hay zonas no puedo hacer nada
  IF numero > 0 THEN
    SELECT COUNT(*) INTO numero FROM 
      zonaurbana z 
      WHERE z.NombreZona=nombreZona;
    -- la zona urbana existe??
    IF (numero=1) THEN
      INSERT INTO casaParticular
        VALUE (nombreZona, numero_entrada, metros);
    ELSE
      -- mensaje a pantalla
      -- SELECT "no existe esa zona urbana";

      -- mensaje a canal de errores
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'no existe esa zona urbana';
    END IF;
  ELSE 
    -- mensaje a pantalla
    -- SELECT "no hay zonas urbanas";
     -- mensaje a canal de errores
      SIGNAL SQLSTATE '45000' 
        SET MESSAGE_TEXT = 'no hay zonas urbanas';
  END IF;
END//
DELIMITER ;

CALL ejercicio5(625, 6, 120);
SELECT * FROM casaparticular c;
SELECT * FROM zonaurbana z;
DELETE FROM zonaurbana;
CALL p2('ccc');


-- otra version
DELIMITER //
CREATE OR REPLACE PROCEDURE p5 ()
BEGIN

  -- creando variables
  DECLARE v1 int;

  DECLARE i int DEFAULT 1;
  -- DECLARE llave int DEFAULT 1;

  -- creo un cursor para la tabla zona urbana
  DECLARE c1 CURSOR FOR
    SELECT
      zu.NombreZona
    FROM ZonaUrbana zu;

  -- control de salida para el cursor
  /*DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET llave = 0;*/

    -- control error de clave duplicada
    DECLARE CONTINUE HANDLER FOR 1062 
      BEGIN
       set i = i - 1; -- si hay error no sumo 1 al contador
      END;
  
  -- abrimos el cursor
  OPEN c1;
  WHILE (i <= f3()) DO
    -- leo el primer registro 
    -- almaceno en v1 la primera zona urbana
    FETCH c1 INTO v1;
    -- introduzco el registro en casaParticular
    INSERT INTO CasaParticular
      VALUES (v1, ROUND(RAND()*100), ROUND(RAND() * 300) + 200);
    -- sumar 1 al contador
    
    -- visor
      SELECT v1,i; -- le damos tiempo a grabar el registro en la tabla
      SET i = i + 1;

  END WHILE;
  -- cierro el cursor
  CLOSE c1;
  SELECT
    CONCAT('Ha insertado ', f3(), ' registros.') salida;
END //
DELIMITER ;

SELECT * FROM zonaurbana z;
DELETE FROM zonaurbana;
CALL p2('a');
SELECT * FROM casaparticular c;
DELETE FROM casaparticular;
CALL p5();
SELECT f3();

  /* Ejercicio 6 
Crear un vista que muestre todos los campos de zona urbana 
junto con los campos numero y m2 de la tabla casa particular 
de los registros asociados
  */
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
VIEW v1
AS
SELECT
  `zu`.`NombreZona` AS `NombreZona`,
  `zu`.`Categoria` AS `Categoria`,
  `c`.`Numero` AS `Numero`,
  `c`.`M2` AS `M2`
FROM (`zonaurbana` `zu`
  JOIN `casaparticular` `c`
    ON ((`zu`.`NombreZona` = `c`.`NombreZona`)));

/**
  Ejercicio 7
  Crear un procedimiento, llamarlo p6, 
  para que introduzca de forma automatica 100 registros 
  en la tabla ZonaUrbana. 
  Para crear valores aleatorios podeis utilizar la 
  funcion predefinida llamada RAND() 
  (esta devuelve un numero entre 0 y 1). 
  Para redondear un numero utilizar la funcion ROUND(numero).
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE p6()
  BEGIN
    DECLARE temp int DEFAULT 0;
    DECLARE error int DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR 1062 SET error=1;
      
    
    WHILE (temp<3) DO
        -- intento introducir el registro
        INSERT INTO ZonaUrbana(NombreZona) 
          VALUES (ROUND(RAND()*10));
        -- ha entrado??
        IF(error=0) THEN 
          SET temp=temp+1;
        ELSE 
          set error=0;
         END IF;
    END WHILE;

  END //
DELIMITER ;

CALL p6();
SELECT * FROM ZonaUrbana;
DELETE FROM zonaurbana;



--
-- Definition for procedure p10
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p10 ()
BEGIN
  DECLARE c varchar(20);
  DECLARE p,
          np int;
  DECLARE i int DEFAULT 0;
  DECLARE v int DEFAULT (SELECT
    COUNT(*)
  FROM BloqueCasas bc);
  DECLARE n int DEFAULT (ROUND(RAND() * v));
  DECLARE continua int DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR 1062 SET continua = 1;
  WHILE (i < 23) DO
    SELECT
      bc.Calle,
      bc.Numero,
      bc.Npisos INTO c, p, np
    FROM BloqueCasas bc LIMIT n, 1;
    INSERT INTO Piso
      VALUES (c, p, ROUND(RAND() * np) + 1, CHAR(ROUND(RAND() * 6) + 65), ROUND(RAND() * 300) + 200);
    WHILE (continua) DO
      SET continua = 0;
      INSERT INTO Piso
        VALUES (c, p, ROUND(RAND() * np) + 1, CHAR(ROUND(RAND() * 6) + 65), ROUND(RAND() * 300) + 200);
    END WHILE;
    SET i = i + 1;
    SET n = ROUND(RAND() * v);
  END WHILE;
END
$$

--
-- Definition for procedure p11
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p11 (fecha date, OUT d int, OUT m int, OUT a int)
BEGIN
  SET d = DAY(fecha);
  SET m = MONTH(fecha);
  SET a = YEAR(fecha);
END
$$





--
-- Definition for procedure p6
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p6 ()
BEGIN
  DECLARE i int DEFAULT 1;
  WHILE (i < 101) DO
    INSERT INTO ZonaUrbana
      VALUES (DEFAULT, CONCAT(CHAR(ROUND(RAND() * (90 - 65)) + 65), CHAR(ROUND(RAND() * (90 - 65)) + 65)));
    SET i = i + 1;
  END WHILE;
END
$$

--
-- Definition for procedure p7
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p7 ()
BEGIN
  DECLARE v int DEFAULT f4();
  DECLARE i int DEFAULT 1;
  WHILE (i < 11) DO
    INSERT INTO CasaParticular
      VALUES (v, ROUND(RAND() * 100), ROUND(RAND() * 300) + 200);
    SET i = i + 1;
    SET v = f4();
  END WHILE;
END
$$

--
-- Definition for procedure p8
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p8 ()
BEGIN
  DECLARE v1,
          i int;
  DECLARE v2 char(2);
  DECLARE c1 CURSOR FOR
  SELECT
    *
  FROM ZonaUrbana zu;
  SET i = 0;
  OPEN c1;
  WHILE (i < 10) DO
    FETCH c1 INTO v1, v2;
    SELECT
      v1 NombreZona,
      v2 Categoria;
    SET i = i + 1;
  END WHILE;
  CLOSE c1;
END
$$

--
-- Definition for procedure p9
--
CREATE DEFINER = 'root'@'localhost'
PROCEDURE p9 ()
BEGIN
  DECLARE v int DEFAULT f4();
  DECLARE i int DEFAULT 0;
  DECLARE continua int DEFAULT 0;
  DECLARE CONTINUE HANDLER FOR 1062 SET continua = 1;
  WHILE (i < 10) DO
    INSERT IGNORE BloqueCasas
      VALUES ('C/ Mayor', ROUND(RAND() * 100), ROUND(RAND() * 7) + 1, v);
    WHILE (continua) DO
      SET continua = 0;
      INSERT INTO BloqueCasas
        VALUES ('C/ Mayor', ROUND(RAND() * 100), ROUND(RAND() * 7) + 1, v);
    END WHILE;
    SET i = i + 1;
    SET v = f4();
  END WHILE;
END
$$

--
-- Definition for function f14
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION f14 (arg varchar(20))
RETURNS varchar(20) charset latin1
BEGIN
  RETURN RTRIM(LTRIM(arg));
END
$$



--
-- Definition for function f4
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION f4 ()
RETURNS int(11)
BEGIN
  DECLARE v int DEFAULT (SELECT
    COUNT(*)
  FROM ZonaUrbana zu);
  DECLARE n int DEFAULT (ROUND(RAND() * v));
  RETURN (SELECT
    zu.NombreZona
  FROM ZonaUrbana zu LIMIT n, 1);
END
$$

--
-- Definition for function f5
--
CREATE DEFINER = 'root'@'localhost'
FUNCTION f5 (fecha date)
RETURNS int(11)
BEGIN
  DECLARE años int DEFAULT 0;
  SET años = YEAR(NOW()) - YEAR(fecha);
  IF (MONTH(NOW()) < MONTH(fecha)) THEN
    SET años = años - 1;
  ELSEIF (MONTH(NOW()) = MONTH(fecha)) THEN
    IF (DAY(NOW()) < DAY(fecha)) THEN
      SET años = años - 1;
    END IF;
  END IF;
  RETURN años;
END
$$

DELIMITER ;



-- 
-- Dumping data for table zonaurbana
--
INSERT INTO zonaurbana VALUES
(1, 'HH'),
(2, 'MN'),
(3, 'GO'),
(4, 'DW'),
(5, 'CV'),
(6, 'WU'),
(7, 'LW'),
(8, 'YE'),
(9, 'WY'),
(10, 'EW'),
(11, 'BL'),
(12, 'EJ'),
(13, 'MF'),
(14, 'QO'),
(15, 'WR'),
(16, 'VB'),
(17, 'TR'),
(18, 'DM'),
(19, 'EJ'),
(20, 'GC'),
(21, 'SL'),
(22, 'XJ'),
(23, 'YV'),
(24, 'FL'),
(25, 'SF'),
(26, 'WV'),
(27, 'ND'),
(28, 'YL'),
(29, 'KP'),
(30, 'VL'),
(31, 'SF'),
(32, 'UM'),
(33, 'BQ'),
(34, 'FC'),
(35, 'SL'),
(36, 'CZ'),
(37, 'SP'),
(38, 'VM'),
(39, 'VT'),
(40, 'IJ'),
(41, 'UY'),
(42, 'JY'),
(43, 'RM'),
(44, 'KO'),
(45, 'OF'),
(46, 'GR'),
(47, 'OW'),
(48, 'QQ'),
(49, 'FE'),
(50, 'FO'),
(51, 'FG'),
(52, 'OD'),
(53, 'WE'),
(54, 'BV'),
(55, 'AN'),
(56, 'QQ'),
(57, 'EA'),
(58, 'ML'),
(59, 'UR'),
(60, 'YR'),
(61, 'PA'),
(62, 'EW'),
(63, 'UL'),
(64, 'RF'),
(65, 'YG'),
(66, 'IU'),
(67, 'DG'),
(68, 'UI'),
(69, 'CN'),
(70, 'KI'),
(71, 'NO'),
(72, 'GP'),
(73, 'HQ'),
(74, 'IS'),
(75, 'OP'),
(76, 'JY'),
(77, 'RP'),
(78, 'XS'),
(79, 'YP'),
(80, 'EZ'),
(81, 'LH'),
(82, 'DU'),
(83, 'NG'),
(84, 'RR'),
(85, 'HK'),
(86, 'DJ'),
(87, 'MH'),
(88, 'XU'),
(89, 'HZ'),
(90, 'DP'),
(91, 'OE'),
(92, 'ZN'),
(93, 'RT'),
(94, 'TO'),
(95, 'NW'),
(96, 'UM'),
(97, 'YJ'),
(98, 'ZV'),
(99, 'FN'),
(100, 'BO');

-- 
-- Dumping data for table bloquecasas
--
INSERT INTO bloquecasas VALUES
('C/ Mayor', 3, 5, 19),
('C/ Mayor', 7, 4, 99),
('C/ Mayor', 9, 8, 50),
('C/ Mayor', 18, 4, 83),
('C/ Mayor', 31, 5, 32),
('C/ Mayor', 60, 4, 52),
('C/ Mayor', 67, 7, 51),
('C/ Mayor', 92, 6, 66),
('C/ Mayor', 93, 4, 8),
('C/ Mayor', 99, 4, 86);

-- 
-- Dumping data for table casaparticular
--
INSERT INTO casaparticular VALUES
(23, 33, 499),
(23, 85, 371),
(24, 67, 403),
(38, 81, 479),
(57, 79, 278),
(60, 30, 415),
(70, 30, 327),
(72, 86, 248),
(96, 94, 465),
(99, 92, 399);

-- 
-- Dumping data for table piso
--
INSERT INTO piso VALUES
('C/ Mayor', 9, 2, 'G', 343),
('C/ Mayor', 9, 3, 'C', 411),
('C/ Mayor', 9, 3, 'D', 443),
('C/ Mayor', 9, 8, 'D', 326),
('C/ Mayor', 18, 1, 'D', 454),
('C/ Mayor', 18, 4, 'B', 301),
('C/ Mayor', 18, 5, 'B', 440),
('C/ Mayor', 31, 2, 'D', 262),
('C/ Mayor', 31, 4, 'B', 352),
('C/ Mayor', 31, 4, 'C', 289),
('C/ Mayor', 60, 3, 'A', 357),
('C/ Mayor', 60, 4, 'B', 403),
('C/ Mayor', 60, 4, 'F', 431),
('C/ Mayor', 60, 5, 'A', 386),
('C/ Mayor', 67, 4, 'B', 393),
('C/ Mayor', 67, 5, 'B', 453),
('C/ Mayor', 67, 6, 'D', 391),
('C/ Mayor', 67, 8, 'A', 290),
('C/ Mayor', 92, 5, 'E', 460),
('C/ Mayor', 93, 3, 'B', 302),
('C/ Mayor', 93, 5, 'E', 202),
('C/ Mayor', 99, 3, 'G', 499),
('C/ Mayor', 99, 4, 'B', 257);

-- 
-- Dumping data for table persona
--

-- Table viviendas.persona does not contain any data (it is empty)

-- 
-- Dumping data for table poseec
--

-- Table viviendas.poseec does not contain any data (it is empty)

-- 
-- Dumping data for table poseep
--

-- Table viviendas.poseep does not contain any data (it is empty)

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;